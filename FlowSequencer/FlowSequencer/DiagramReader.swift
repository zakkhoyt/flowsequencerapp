//
//  FlowDecoder.swift
//  FlowSequencer
//
//  Created by Zakk Hoyt on 12/21/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import Foundation

public class DiagramReader {
    public enum Errors: Error {
        case notFound
    }
    
    public var diagram = Diagram()
    
    public init(name: String) throws {
        let contents = try contentsOf(name: name)
        iterate(contents: contents)
    }
    
    private func contentsOf(name: String) throws -> String {
        guard let path = Bundle.main.path(forResource: name, ofType: "txt") else {
            throw Errors.notFound
        }
        
        let url = URL(fileURLWithPath: path)
        let contents = try String(contentsOf: url)
        return contents
    }
    
    private func iterate(contents: String) {
        
        let lines = contents.split(separator: "\n")
        for line in lines {
            if let relationship = process(line: String(line)) {
                diagram.relationships.append(relationship)
            }
        }
    }
    
    private func process(line: String) -> Relationship? {
        let words = line.split(separator: " ")
        if words.count != 3 {
            print("Warning: invalid number of words: \(words.count)")
            return nil
        }

        guard let from = process(word: String(words[0])) as? Object else {
            return nil
        }
        guard let relates = process(word: String(words[1])) as? Relates else {
            return nil
        }
        guard let to = process(word: String(words[2])) as? Object else {
            return nil
        }
        
        return Relationship(from: from, relates: relates, to: to)


    }
    
    private func process(word: String) -> Drawable {
        guard let range = word.range(of: "?") else {
            let name = word
            return process(name: name)
        }
        
        let nameRange: Range<String.Index> = {
            let start = word.startIndex
            let end = range.lowerBound
            return start..<end
        }()
        let parametersRange: Range<String.Index> = {
            let start = range.lowerBound
            let end = word.endIndex
            return start..<end
        }()
        
        let name = String(word[nameRange])
        let parameters = String(word[parametersRange])
        let components = URLComponents(string: parameters)
        let queryItems = components?.queryItems
        
        return process(name: name, queryItems: queryItems)
    }
    
    
    
    private func process(name: String, queryItems: [URLQueryItem]? = nil) -> Drawable {
        var drawable: Drawable = {
            if let arrow = Relates.Arrow(rawValue: name) {
                return Relates(arrow: arrow)
            } else {
                if let object = diagram.objectOf(name: name) {
                    return object
                }
                return Object(name: name)
            }
        }()
        
        defer { // Look! A use for defer!
            if let object = drawable as? Object {
                diagram.updateObject(name: name, with: object)
            }
        }
        
        guard let queryItems = queryItems else {
            return drawable
        }
        
        for queryItem in queryItems {
            guard let value = queryItem.value else {
                print("Warning: queryItem does not contain a value: \(queryItem.name)")
                continue
            }
            guard let constant = Constants(rawValue: queryItem.name) else {
                print("Warning: undefined Constant: \(queryItem.name)")
                continue
            }
            
            switch constant {
            case .label:
                drawable.label = value
            case .stroke:
                let strokeColor = UIColor(hexString: value)
                drawable.strokeColor = strokeColor
            case .fill:
                let fillColor = UIColor(hexString: value)
                drawable.fillColor = fillColor
            case .width:
                if let intValue = Int(value) {
                    let lineWidth = CGFloat(intValue)
                    drawable.lineWidth = lineWidth
                }
            case .font:
                if let intValue = Int(value) {
                    let fontSize = CGFloat(intValue)
                    drawable.font = UIFont.systemFont(ofSize: fontSize)
                }
            }
        }
        return drawable
    }
}
