//
//  DiagramView.swift
//  FlowSequencer
//
//  Created by Zakk Hoyt on 12/21/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        return ceil(boundingBox.height)
    }
    
    func width(withConstrainedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
        return ceil(boundingBox.width)
    }
    
    func size(withConstrainedSize size: CGSize, font: UIFont) -> CGSize {
        let width = self.width(withConstrainedHeight: size.width, font: font)
        let height = self.height(withConstrainedWidth: size.height, font: font)
        return CGSize(width: width, height: height)
    }
}


public class DiagromCompositor {
    private(set) var diagram: Diagram
    var size: CGSize {
        return masterRect.size
    }
    private var masterRect: CGRect = .zero
    
    init(diagram: Diagram) {
        self.diagram = diagram
        self.update()
    }
    
    private func update() {
        
        //let objectHeaderHeight: CGFloat = 64   // will be calculate
//        let objectHeaderWidth: CGFloat = 128
        let xSpacing: CGFloat = 400             // will be calculate
        let halfXSpacing = xSpacing / 2.0
        let ySpacing: CGFloat = 200
        let halfYSpacing = ySpacing / 2.0
        let width: CGFloat = 1
        let halfWidth = width / 2.0
        let height: CGFloat = 1
        let halfHeight = height / 2.0
        
        do {
            // Set frame of each object based on relations
            for (x, object) in diagram.objects.enumerated() {
                
//                object.headerRect = CGRect(x: halfXSpacing - (objectHeaderWidth / 2.0) + CGFloat(x) * xSpacing,
//                                           y: 0,
//                                           width: objectHeaderWidth,
//                                           height: objectHeaderHeight)
//
                let headerSize = object.nameSize
                object.headerRect =  CGRect(x: halfXSpacing - (headerSize.width / 2.0) + CGFloat(x) * xSpacing,
                                            y: 0,
                                            width: headerSize.width,
                                            height: headerSize.height).insetBy(dx: -16, dy: 0)
                

                object.trackRect = CGRect(x: halfXSpacing - halfWidth + CGFloat(x) * xSpacing,
                                          y: object.headerRect.origin.y + object.headerRect.height,
                                          width: width,
                                          height: object.headerRect.height)
                
                
                object.rect = object.trackRect.union(object.headerRect)
            }
        }

        do {
            for (y, relationship) in diagram.relationships.enumerated() {
                
                let x1: CGFloat = relationship.from.trackRect.origin.x + relationship.from.trackRect.width
                let x2: CGFloat = relationship.to.trackRect.origin.x
                relationship.relates.rect = CGRect(x: x1,
                                                   y: relationship.from.headerRect.height + halfYSpacing - halfHeight + CGFloat(y) * ySpacing,
                                                   width: x2 - x1,
                                                   height: height)
                
                masterRect = masterRect.union(relationship.relates.rect)
            }
        }

        // Go back and set the proper height
        for object in diagram.objects {
            let trackRect = CGRect(x: object.trackRect.origin.x,
                                   y: object.trackRect.origin.y,
                                   width: object.trackRect.width,
                                   height: masterRect.size.height - object.trackRect.origin.y + halfYSpacing)
            object.trackRect = trackRect
        }
        
        // add bottom and right gutter
        masterRect = CGRect(x: masterRect.origin.x,
                            y: masterRect.origin.y,
                            width: masterRect.width + halfXSpacing,
                            height: masterRect.height + halfYSpacing)
    }
}

open class DiagramView: UIView {
    
    private var compositor: DiagromCompositor?
    public var diagram: Diagram? {
        didSet {
            guard let diagram = diagram else { return }
            compositor = DiagromCompositor(diagram: diagram)
            setNeedsLayout()
            setNeedsDisplay()
        }
    }
    
    
    
    open override func layoutSubviews() {
        super.layoutSubviews()
        
        guard let compositor = compositor else { return }
        backgroundColor = UIColor(hexString: "FF9999")
        translatesAutoresizingMaskIntoConstraints = false
        widthAnchor.constraint(equalToConstant: compositor.size.width).isActive = true
        heightAnchor.constraint(equalToConstant: compositor.size.height).isActive = true
    }
    
    // https://stackoverflow.com/questions/38961499/how-to-make-a-dashed-line-in-swift
    open override func draw(_ rect: CGRect) {
        
        guard let diagram = diagram else { return }
        
        for object in diagram.objects {
            func drawHeader() {
                object.fillColor.setFill()
                object.strokeColor.setStroke()
                let  headerPath = UIBezierPath(rect: object.headerRect)
                headerPath.lineWidth = object.lineWidth
                headerPath.stroke()
                
                func drawText() {
                    let name = object.name as NSString
                    let attributes: [NSAttributedString.Key: Any] = [
                        .font: object.font,
                        .foregroundColor: object.strokeColor
                    ]
                    let frame = CGRect(x: object.headerRect.origin.x + (object.headerRect.width - object.nameSize.width) / 2.0,
                                       y: object.headerRect.origin.y + (object.headerRect.height - object.nameSize.height) / 2.0,
                                       width: object.nameSize.width,
                                       height: object.nameSize.height)
                    name.draw(with: frame,
                              options: .usesLineFragmentOrigin,
                              attributes: attributes,
                              context: nil)
                }
                drawText()
            }
            drawHeader()
            
            func drawTrack() {
                object.fillColor.setFill()
                object.strokeColor.setStroke()
                let  trackPath = UIBezierPath(rect: object.trackRect)
                trackPath.lineWidth = object.lineWidth
                trackPath.stroke()
            }
            drawTrack()
        }
        
        for relationship in diagram.relationships {
            relationship.relates.fillColor.setFill()
            relationship.relates.strokeColor.setStroke()
            let path = UIBezierPath(rect: relationship.relates.rect)
            relationship.relates.arrow.configureLine(path: path)
            let x1: CGFloat = relationship.relates.rect.origin.x
            let x2: CGFloat = x1 + relationship.relates.rect.size.width
            let y: CGFloat = relationship.relates.rect.origin.y + relationship.relates.rect.height / 2.0
            path.move(to: CGPoint(x: x1, y: y))
            path.addLine(to: CGPoint(x: x2, y: y))
            //path.close()
            path.stroke()
        }
    }
    
}
