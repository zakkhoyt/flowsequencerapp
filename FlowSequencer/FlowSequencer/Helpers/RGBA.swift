//
//  RGBA.swift
//  Throw
//
//  Created by Zakk Hoyt on 10/22/16.
//  Copyright © 2016 Zakk Hoyt. All rights reserved.
//

import UIKit


public struct RGBA {

    public var red: CGFloat
    public var green: CGFloat
    public var blue: CGFloat
    public var alpha: CGFloat
    
    public init(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) {
        func clip(_ value: CGFloat) -> CGFloat {
            return max(0.0, min(1.0, value))
        }
        self.red = clip(red)
        self.green = clip(green)
        self.blue = clip(blue)
        self.alpha = clip(alpha)
    }
    
    public init(red: CGFloat, green: CGFloat, blue: CGFloat) {
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
    
    public func description() -> String {
        return "red: " + String(format: "%.2f", red) +
        "green: " + String(format: "%.2f", green) +
        "blue: " + String(format: "%.2f", blue) +
        "alpha: " + String(format: "%.2f", alpha)
    }
    
    public func color() -> UIColor {
        let color = UIColor(red: red, green: green, blue: blue, alpha: alpha)
        return color
    }
    
    // MARK: Static functions
    
    public static func colorWith(rgba: RGBA) -> UIColor {
        return rgba.color()
    }
}

extension UIColor {
    
    // MARK: UIColor to self
    public func rgba(alpha: CGFloat = 1.0) -> RGBA {
        var red: CGFloat = 0
        var green: CGFloat = 0
        var blue: CGFloat = 0
        var alpha: CGFloat = 0
        
        self.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        return RGBA(red: red, green: green, blue: blue, alpha: alpha)
    }
    
    // MARK: constructors
    
    public convenience init(rgba: RGBA, alpha: CGFloat = 1.0) {
        self.init(red: rgba.red, green: rgba.green, blue: rgba.blue, alpha: rgba.alpha)
    }

    public class func colorWith(rgba: RGBA) -> UIColor {
        return rgba.color()
    }
    
}

extension UIImage {
    public func rgbaPixels() -> [RGBA] {
        
        let pixelData = self.cgImage!.dataProvider!.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        let length = CFDataGetLength(pixelData)
        let bytesPerRow = Int(length / Int(size.height))
        
        var pixels = [RGBA]()
        for y in 0 ..< Int(size.width) {
            for x in 0 ..< Int(size.height) {
                let index = y * bytesPerRow + x * 4
                
                if index < 0 {
                    print("Error: index out of bounds")
                    break
                } else if index > Int(Int(size.width) * Int(size.height) * 4) {
                    print("Error: index out of bounds")
                    break
                }
                
                //        print("w:\(Int(size.width)) h:\(Int(size.height))")
                //        print("x:\(Int(point.x)) y:\(Int(point.y)) index:\(index)")
                let blue = CGFloat(data[index]) / CGFloat(255.0)
                let green = CGFloat(data[index+1]) / CGFloat(255.0)
                let red = CGFloat(data[index+2]) / CGFloat(255.0)
                let alpha = CGFloat(data[index+3]) / CGFloat(255.0)
                
                let rgba = RGBA(red: red, green: green, blue: blue, alpha: alpha)
                pixels.append(rgba)
            }
        }
        
        return pixels
    }
}
