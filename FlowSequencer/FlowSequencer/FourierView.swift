//
//  FourierView.swift
//  FlowSequencer
//
//  Created by Zakk Hoyt on 12/21/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//
// https://www.youtube.com/watch?v=WSkczQe6YxA
//
//  Radians: https://upload.wikimedia.org/wikipedia/commons/4/4e/Circle_radians.gif
//  Brachist: https://i.imgur.com/cxpk0GD.gif
// Sin/Cos: https://i.imgur.com/uPpkWr9.gif
// Sin/Cos: https://i.imgur.com/5fpYND4.gif
// Tan: https://i.imgur.com/9JJEgo8.gif
// Polar: https://i.imgur.com/UBbzD8Z.gif
// Trig: https://i.imgur.com/2AvSwEA.mp4
// Group: https://www.reddit.com/r/educationalgifs/comments/2c7gl3/trig_functions_visualized_xpost_from_mathgifs/?st=jpzy34zf&sh=3728597f
// Trig app: https://www.mathwarehouse.com/animated-gifs/trigonometry.php


import UIKit

open class FourierView: UIView {
    public enum Waveform: Int, CaseIterable {
        case square
        case triangle
        case sawtooth
        case semicircle
        
        public var title: String {
            switch self {
            case .square: return "Square"
            case .triangle: return "Triangle"
            case .sawtooth: return "Sawtooth"
            case .semicircle: return "Semicircle"
            }
        }
    }
    
    public var waveform: Waveform = .square
    public var frequency: TimeInterval = 0.2
    public var iterations: Int = 16

    private var yArchive: [CGFloat] = []
    private var startDate = Date()
    private var elapsed: TimeInterval {
        return Date().timeIntervalSince(startDate)
    }
    private var unitCenter: CGPoint {
        return CGPoint(x: bounds.width / 4.0, y: bounds.height / 2.0)
    }
    private var unitRadius: CGFloat {
        return bounds.width / 8.0
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        let link = CADisplayLink(target: self, selector: #selector(update))
        link.add(to: .current, forMode: .common)
        
        backgroundColor = .black
    }
    
    @objc private func update() {
        setNeedsDisplay()
    }
    
    
    open override func draw(_ rect: CGRect) {
        func drawCircle(center: CGPoint, radius: CGFloat) {
            UIColor.lightGray.setStroke()
            let path = UIBezierPath(arcCenter: center,
                                    radius: radius,
                                    startAngle: 0,
                                    endAngle: 2 * CGFloat.pi,
                                    clockwise: true)
            path.lineWidth = 0.5
            path.stroke()
        }
        
        func drawRadius(point1: CGPoint, point2: CGPoint) {
            UIColor.yellow.setStroke()
            let path = UIBezierPath()
            path.move(to: point1)
            path.addLine(to: point2)
            path.lineWidth = 0.5
            path.stroke()
        }
        
        func drawText(string: String) {
            let attributes: [NSAttributedString.Key: Any] = [
                .font: UIFont.systemFont(ofSize: 14, weight: .medium),
                .foregroundColor: UIColor.orange
            ]
            
            let frame = CGRect(x: bounds.width - bounds.width / 4.0,
                               y: 0,
                               width: bounds.width / 4.0,
                               height: bounds.height / 8.0)
            
            let text = string as NSString
            text.draw(with: frame,
                      options: .usesLineFragmentOrigin,
                      attributes: attributes,
                      context: nil)
        }
        
        
        var tangentPoint: CGPoint = .zero
        for i in 0..<iterations {
            let duration = 1.0 / frequency
            let percent = CGFloat(elapsed.truncatingRemainder(dividingBy: duration) / duration)
            let phase = 2.0 * CGFloat.pi * percent
            let n = CGFloat(i)
            
            func calculate() -> (c: CGFloat, x: CGFloat, y: CGFloat) {
                let c: CGFloat
                let x: CGFloat
                let y: CGFloat
                switch waveform {
                case .square:
                    c = 1.0 / (2.0 * n + 1)
                    x = c * unitRadius * cos((2 * n + 1) * phase)
                    y = c * unitRadius * sin((2 * n + 1) * phase)
                case .triangle:
                    let cTop: CGFloat = pow(-1, n)
                    let cBottom: CGFloat = pow(2.0 * n + 1.0, 2)
                    c = cTop / cBottom
                    x = c * unitRadius * cos((2.0 * n + 1) * phase)
                    y = c * unitRadius * sin((2.0 * n + 1) * phase)
                case .sawtooth:
                    c = i % 2 == 0 ? (-1.0 / (n + 1)) : (1.0 / (n + 1))
                    x = c * unitRadius * cos((n + 1) * phase)
                    y = c * unitRadius * sin((n + 1) * phase)
                case .semicircle:
                    // This one doesn't work right. More of a triangle alternative
                    c = 1.0 / (2.0 * n + 1)
                    x = 1.0 / CGFloat(n + 1) * c * unitRadius * sin((2 * n + 1) * phase)
                    y = 1.0 / CGFloat(n + 1) * c * unitRadius * cos((2 * n + 1) * phase)
                }
                return (c, x, y)
            }
            let cxy = calculate()
            if i == 0 {
                tangentPoint = CGPoint(x: unitCenter.x + cxy.x, y: unitCenter.y + cxy.y)
                drawCircle(center: unitCenter, radius: unitRadius)
                drawRadius(point1: unitCenter, point2: tangentPoint)
            } else {
                let previousTangentPoint = tangentPoint
                tangentPoint = CGPoint(x: tangentPoint.x + cxy.x, y: tangentPoint.y + cxy.y)
                drawCircle(center: previousTangentPoint, radius: unitRadius * cxy.c)
                drawRadius(point1: previousTangentPoint, point2: tangentPoint)
            }
        }
        
        UIColor.cyan.setStroke()
        let bridgePath = UIBezierPath()
        bridgePath.lineWidth = 0.5
        bridgePath.move(to: tangentPoint)
        let drawPoint = CGPoint(x: unitCenter.x + 2 * unitRadius,
                                y: tangentPoint.y)
        bridgePath.addLine(to: drawPoint)
        bridgePath.stroke()
        
        yArchive.insert(tangentPoint.y, at: 0)
        if yArchive.count > Int(bounds.width / UIScreen.main.scale) {
            yArchive.removeLast()
        }
        
        let unitY: CGFloat = (unitCenter.y - tangentPoint.y) / unitRadius
        drawText(string: "\(unitY)")
        
        UIColor.yellow.setStroke()
        let historyPath = UIBezierPath()
        for (x, y) in yArchive.enumerated() {
            let point = CGPoint(x: drawPoint.x + CGFloat(x), y: CGFloat(y))
            if x == 0 {
                historyPath.move(to: point)
            } else {
                historyPath.addLine(to: point)
            }
        }
        historyPath.stroke()
    }
}
