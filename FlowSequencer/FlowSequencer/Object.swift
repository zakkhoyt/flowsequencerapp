//
//  Instance.swift
//  FlowSequencer
//
//  Created by Zakk Hoyt on 12/21/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

//public struct Object: Codable, Hashable {
//
//    public enum Shape: String, Codable {
//        case rectangle = "rectangle"
//        case circle = "circle"
//    }
//
//    public var name: String
//    public var alias: String
//    public var strokeColor: UIColor
//    public var fillColor: UIColor
//    public var lineWidth: CGFloat
//
//    public var shape: Object.Shape
//
//    private enum CodingKeys: String, CodingKey {
//        case name = "name"
//        case alias = "alias"
//        case shape = "shape"
//        case strokeColor = "stroke"
//        case fillColor = "fill"
//        case lineWidth = "line"
//    }
//
//
//    public init(from decoder: Decoder) throws {
//        let container = try decoder.container(keyedBy: CodingKeys.self)
//
//
//        self.name = try container.decode(String.self, forKey: .name)
//        self.alias = try container.decode(String.self, forKey: .alias)
//        let strokeHexString = try container.decode(String.self, forKey: .strokeColor)
//        self.strokeColor = UIColor(hexString: strokeHexString)
//        let fillHexString = try container.decode(String.self, forKey: .fillColor)
//        self.fillColor = UIColor(hexString: fillHexString)
//        self.lineWidth = try container.decode(CGFloat.self, forKey: .lineWidth)
//
//        self.shape = try container.decode(Object.Shape.self, forKey: .shape)
//    }
//
//    public func encode(to encoder: Encoder) throws {
//        var container = encoder.container(keyedBy: CodingKeys.self)
//
//        try container.encode(self.name, forKey: .name)
//        try container.encode(self.alias, forKey: .alias)
//        try container.encode(self.strokeColor.hexString, forKey: .strokeColor)
//        try container.encode(self.fillColor.hexString, forKey: .fillColor)
//        try container.encode(self.lineWidth, forKey: .lineWidth)
//
//        try container.encode(self.shape, forKey: .shape)
//    }
//
//    public var hashValue: Int {
//        return name.hashValue
//    }
//}

