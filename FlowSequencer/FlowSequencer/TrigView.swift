//
//  TrigView.swift
//  FlowSequencer
//
//  Created by Zakk Hoyt on 12/22/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//
//


// Trig app: https://www.mathwarehouse.com/animated-gifs/trigonometry.php


import UIKit

open class TrigView: UIView {
    public enum TrigFunction: Int, CaseIterable {
        case sin
        case cos
        case tan
        case sec
        case csc
        case cot
        
        public static let all: Set<TrigFunction> = [.sin, .cos, .tan, .sec, .csc, .cot]
        
        public var title: String {
            switch self {
            case .sin: return "sin"
            case .cos: return "cos"
            case .tan: return "tan"
            case .sec: return "sec"
            case .csc: return "csc"
            case .cot: return "cot"
            }
        }
        
        public var strokeColor: UIColor {
            switch self {
            case .sin: return .cyan
            case .cos: return .orange
            case .tan: return .green
            case .sec: return .magenta
            case .csc: return .yellow
            case .cot: return .purple
            }
        }
    }
    
    public var trigFunctions: Set<TrigFunction> = TrigFunction.all
    public var frequency: TimeInterval = 0.1
    
    private var yArchive: [CGFloat] = []
    private var startDate = Date()
    private var elapsed: TimeInterval {
        return Date().timeIntervalSince(startDate)
    }
    private var unitCenter: CGPoint {
        return CGPoint(x: bounds.width / 4.0, y: bounds.height / 2.0)
    }
    private var unitRadius: CGFloat {
        return bounds.width / 8.0
    }
    
    open override func awakeFromNib() {
        super.awakeFromNib()
        let link = CADisplayLink(target: self, selector: #selector(update))
        link.add(to: .current, forMode: .common)
        
        backgroundColor = .black
    }
    
    @objc private func update() {
        setNeedsDisplay()
    }
    
    
    open override func draw(_ rect: CGRect) {
        func drawCircle(center: CGPoint, radius: CGFloat) {
            UIColor.lightGray.setStroke()
            let path = UIBezierPath(arcCenter: center,
                                    radius: radius,
                                    startAngle: 0,
                                    endAngle: 2 * CGFloat.pi,
                                    clockwise: true)
            path.lineWidth = 0.5
            path.stroke()
        }
        
        func drawRadius(point1: CGPoint,
                        point2: CGPoint) {
            let path = UIBezierPath()
            path.move(to: point1)
            path.addLine(to: point2)
            path.lineWidth = 0.5
            path.stroke()
        }
        
        func drawText(text: String,
                      textColor: UIColor,
                      at point: CGPoint) {
            let attributes: [NSAttributedString.Key: Any] = [
                .font: UIFont.systemFont(ofSize: 12, weight: .thin),
                .foregroundColor: textColor
            ]
            let string = text as NSString
            let size = string.size(withAttributes: attributes)
            let frame = CGRect(x: point.x - size.width / 2.0,
                               y: point.y - size.height / 2.0,
                               width: size.width,
                               height: size.height)
            string.draw(with: frame,
                      options: .usesLineFragmentOrigin,
                      attributes: attributes,
                      context: nil)
        }
        
        
        func pointBetween(point1: CGPoint,
                          point2: CGPoint) -> CGPoint {
            let x = point1.x + (point2.x - point1.x) / 2.0
            let y = point1.y + (point2.y - point1.y) / 2.0
            return CGPoint(x: x, y: y)
        }

        func drawText(text: String,
                      textColor: UIColor,
                      between point1: CGPoint,
                      and point2: CGPoint) {
            let point = pointBetween(point1: point1, point2: point2)
            drawText(text: text, textColor: textColor, at: point)
        }
        
        func drawTrigFunctionTitle(trigFunction: TrigFunction,
                                   between point1: CGPoint,
                                   and point2: CGPoint) {
            drawText(text: trigFunction.title,
                     textColor: trigFunction.strokeColor,
                     between: point1,
                     and: point2)
        }

        
        let duration = 1.0 / frequency
        let percent = CGFloat(elapsed.truncatingRemainder(dividingBy: duration) / duration)
        let phase = 2.0 * CGFloat.pi * percent
        
        func drawUnitCircle() {
            let x = unitRadius * cos(phase)
            let y = unitRadius * sin(phase)
            let tangentPoint = CGPoint(x: unitCenter.x + x, y: unitCenter.y + y)
            UIColor.yellow.setStroke()
            drawCircle(center: unitCenter, radius: unitRadius)
            drawRadius(point1: unitCenter, point2: tangentPoint)
            
            
            if trigFunctions.contains(.sin) {
                TrigFunction.sin.strokeColor.setStroke()
                let point2 = CGPoint(x: tangentPoint.x, y: unitCenter.y)
                drawRadius(point1: tangentPoint, point2: point2)
                drawTrigFunctionTitle(trigFunction: .sin, between: tangentPoint, and: point2)
            }
            
            if trigFunctions.contains(.cos) {
                TrigFunction.cos.strokeColor.setStroke()
                let point2 = CGPoint(x: unitCenter.x, y: tangentPoint.y)
                drawRadius(point1: tangentPoint, point2: point2)
                drawTrigFunctionTitle(trigFunction: .cos, between: tangentPoint, and: point2)
            }
            
            if trigFunctions.contains(.tan) {
                TrigFunction.tan.strokeColor.setStroke()
                let theta = atan(y / x)
                let xx = y * tan(theta)
                let point2 = CGPoint(x: tangentPoint.x + xx,
                                     y: unitCenter.y)
                drawRadius(point1: tangentPoint, point2: point2)
                drawTrigFunctionTitle(trigFunction: .tan, between: tangentPoint, and: point2)
            }
            
            if trigFunctions.contains(.sec) {
                TrigFunction.sec.strokeColor.setStroke()
                let xx = unitRadius * 1 / cos(phase)
                let point2 = CGPoint(x: unitCenter.x + xx, y: unitCenter.y)
                drawRadius(point1: unitCenter, point2: point2)
                drawTrigFunctionTitle(trigFunction: .sec, between: unitCenter, and: point2)
            }
            
            if trigFunctions.contains(.csc) {
                TrigFunction.csc.strokeColor.setStroke()
                let yy = unitRadius * 1 / sin(phase)
                let point2 = CGPoint(x: unitCenter.x, y: unitCenter.y + yy)
                drawRadius(point1: unitCenter, point2: point2)
                drawTrigFunctionTitle(trigFunction: .csc, between: unitCenter, and: point2)
            }
            
            if trigFunctions.contains(.cot) {
                TrigFunction.cot.strokeColor.setStroke()
                let theta = atan(x / y)
                let yy = x * tan(theta)
                let point2 = CGPoint(x: unitCenter.x,
                                     y: tangentPoint.y + yy)
                drawRadius(point1: tangentPoint, point2: point2)
                drawTrigFunctionTitle(trigFunction: .cot, between: tangentPoint, and: point2)
            }
        }
        drawUnitCircle()
        
        func drawGraph() {
            let graphRect = CGRect(x: bounds.width / 2,
                                   y: (bounds.height - 2 * unitRadius) / 2.0,
                                   width: bounds.width / 2.0 - 44,
                                   height: unitRadius * 2.0)
            
            UIColor.lightGray.withAlphaComponent(0.1).setFill()
            let graphPath = UIBezierPath(rect: graphRect)
            graphPath.fill()
            
            let quarterWidth = graphRect.width / 4.0
            
            func drawYAxis(xOffset: CGFloat, lineWidth: CGFloat) {
                UIColor.darkGray.setStroke()
                let path = UIBezierPath()
                path.lineWidth = lineWidth
                path.move(to: CGPoint(x: graphRect.origin.x + xOffset,
                                      y: graphRect.origin.y))
                path.addLine(to: CGPoint(x: graphRect.origin.x + xOffset,
                                         y: graphRect.origin.y + graphRect.height))
                path.stroke()
            }
            drawYAxis(xOffset: 0 * quarterWidth, lineWidth: 1)
            drawYAxis(xOffset: 1 * quarterWidth, lineWidth: 0.5)
            drawYAxis(xOffset: 2 * quarterWidth, lineWidth: 0.5)
            drawYAxis(xOffset: 3 * quarterWidth, lineWidth: 0.5)
            drawYAxis(xOffset: 4 * quarterWidth, lineWidth: 0.5)
            
            func drawXAxis() {
                UIColor.darkGray.setStroke()
                let path = UIBezierPath()
                path.lineWidth = 0.5
                path.move(to: CGPoint(x: graphRect.origin.x,
                                      y: graphRect.origin.y + graphRect.height / 2.0))
                path.addLine(to: CGPoint(x: graphRect.origin.x + graphRect.width,
                                         y: graphRect.origin.y + graphRect.height / 2.0))
                path.stroke()
            }
            drawXAxis()
            
            func drawGraphLabel(label: String, xOffset: CGFloat) {
                let attributes: [NSAttributedString.Key: Any] = [
                    .font: UIFont.systemFont(ofSize: 14, weight: .thin),
                    .foregroundColor: UIColor.white
                ]
                let text = label as NSString
                let size = text.size(withAttributes: attributes)
                let point = CGPoint(x: graphRect.origin.x + xOffset,
                                    y: graphRect.origin.y + graphRect.height + 8)
                let frame = CGRect(x: point.x - size.width / 2.0,
                                   y: point.y - size.height / 2.0,
                                   width: size.width,
                                   height: size.height)
                text.draw(with: frame,
                          options: .usesLineFragmentOrigin,
                          attributes: attributes,
                          context: nil)
                
            }
            
            
            drawGraphLabel(label: "0", xOffset: 0 * quarterWidth)
            drawGraphLabel(label: "π/2", xOffset: 1 * quarterWidth)
            drawGraphLabel(label: "π", xOffset: 2 * quarterWidth)
            drawGraphLabel(label: "3π/2", xOffset: 3 * quarterWidth)
            drawGraphLabel(label: "2π", xOffset: 4 * quarterWidth)
            
            func plotTrigFunctions() {
                func plot(trigFunction: TrigFunction, calculate: ((_ phase: CGFloat) -> CGFloat)) {
                    trigFunction.strokeColor.setStroke()
                    
                    let path = UIBezierPath()
                    path.lineWidth = 0.5
                    for x in 0..<Int(graphRect.width) {
                        let phase = 2.0 * CGFloat.pi * CGFloat(x) / graphRect.width
                        var y = graphRect.height / 2.0 * calculate(phase)
                        // Ensure that y is no inf, -inf, NaN, etc....
                        if y.isInfinite || y.isNaN {
                            if y < 0 {
                                y = -CGFloat.greatestFiniteMagnitude
                            } else {
                                y = CGFloat.greatestFiniteMagnitude
                            }
                        }
                        let point = CGPoint(x: graphRect.origin.x + CGFloat(x),
                                            y: graphRect.origin.y + graphRect.height / 2.0 - y)
                        if x == 0 {
                            path.move(to: point)
                        } else {
                            path.addLine(to: point)
                        }
                    }
                    path.stroke()
                    
                    
                    // draw dot
                    trigFunction.strokeColor.setFill()
                    let percent = phase / (2.0 * CGFloat.pi)
                    let x = percent * graphRect.width
                    let y = (graphRect.height / 2.0 * calculate(phase))
                    let point = CGPoint(x: graphRect.origin.x + x,
                                        y: graphRect.origin.y + graphRect.height / 2.0 - y)
                    let dotPath = UIBezierPath(arcCenter: point,
                                               radius: 4,
                                               startAngle: 0,
                                               endAngle: 2.0 * CGFloat.pi,
                                               clockwise: true)
                    dotPath.fill()
                    
                    drawText(text: trigFunction.title,
                             textColor: trigFunction.strokeColor,
                             at: CGPoint(x: point.x, y: point.y + 12))
                }
                
                if trigFunctions.contains(.sin) {
                    plot(trigFunction: .sin) { return sin($0) }
                }
                if trigFunctions.contains(.cos) {
                    plot(trigFunction: .cos) { return cos($0) }
                }
                if trigFunctions.contains(.tan) {
                    plot(trigFunction: .tan) { return tan($0) }
                }
                if trigFunctions.contains(.sec) {
                    plot(trigFunction: .sec) { return 1 / cos($0) }
                }
                if trigFunctions.contains(.csc) {
                    plot(trigFunction: .csc) { return 1 / sin($0) }
                }
                if trigFunctions.contains(.cot) {
                    plot(trigFunction: .cot) { return 1 / tan($0) }
                }
            }
            plotTrigFunctions()
        }
        drawGraph()
    }
}

