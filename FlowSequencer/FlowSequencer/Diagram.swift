//
//  Diagram.swift
//  FlowSequencer
//
//  Created by Zakk Hoyt on 12/21/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit

//public protocol Drawable: Hashable {
public protocol Drawable {
    var hash: String { get set }
    var name: String { get set }
    var label: String? { get set }
    var strokeColor: UIColor { get set }
    var fillColor: UIColor { get set }
    var lineWidth: CGFloat { get set }
    var font: UIFont { get set }
    
    var rect: CGRect { get set }
}

extension Drawable {
    public var hashValue: Int {
        return hash.hashValue
    }
}

extension Drawable {
    var nameSize: CGSize {
        return name.size(withConstrainedSize: CGSize(width: 512, height: 44), font: self.font)
    }
}

extension Drawable {
    public var description: String {
        var output = ""
        
        // TODO: Use URLQueryItems to print (we use the to read so why not use them to print too?)
        let name = self.name
        output += "\(name)?"

        if let label = self.label {
            output += "\(Constants.label.rawValue)=\(label)&"
        }
        
        let stroke = self.strokeColor.hexString
        output += "\(Constants.stroke.rawValue)=\(stroke)&"

        let fill = self.fillColor.hexString
        output += "\(Constants.fill.rawValue)=\(fill)&"

        let width = self.lineWidth
        output += "\(Constants.width.rawValue)=\(Int(width))&"

        let fontSize = self.font.pointSize
        output += "\(Constants.font.rawValue)=\(Int(fontSize))"
        
        return output
    }
}

public class Object: Drawable {
    public enum Shape: String, Codable {
        case rectangle = "rectangle"
        case circle = "circle"
    }
    
    public var hash: String
    public var name: String
    public var label: String?
    public var strokeColor: UIColor = .black
    public var fillColor: UIColor = .clear
    public var lineWidth: CGFloat = 1
    public var font: UIFont = UIFont.systemFont(ofSize: 12, weight: .medium)
    public var rect: CGRect = .zero
    public var shape: Object.Shape = .rectangle
    
    public var headerRect: CGRect = .zero
    public var trackRect: CGRect = .zero
    
    public init(name: String) {
        self.name = name
        self.hash = name
    }
}

public struct Relates: Drawable {
    public enum Arrow: String, CaseIterable {
        case solid = "->"
        case dashed = "-->"
        case dotted = "..>"
        case reverseSolid = "<-"
        case reverseDashed = "<--"
        case reverseDotted = "<.."
        case bidirectionalSolid = "<->"
        case bidirectionalDashed = "<-->"
        case bidirectionalDotted = "<..>"
        
        func configureLine(path: UIBezierPath) {
            
            switch self  {
//            case .solid:
//                path.setLineDash(nil, count: 0, phase: 0)
//            case .dashed:
//                let dashes: [CGFloat] = [10, 10]
//                path.setLineDash(dashes, count: dashes.count, phase: 0)
//                path.lineJoinStyle = .bevel
//                path.lineCapStyle = .round
//            case .dotted:
//                let dashes: [CGFloat] = [8, 8, 8]
//                path.setLineDash(dashes, count: dashes.count, phase: 16.0)
//                path.lineJoinStyle = .round
            default:
                path.setLineDash(nil, count: 0, phase: 0)
            }
        }
    }
    public var arrow: Arrow
    
    public var hash: String
    public var name: String
    public var label: String?
    public var strokeColor: UIColor = .black
    public var fillColor: UIColor = .clear
    public var lineWidth: CGFloat = 1
    public var font: UIFont = UIFont.systemFont(ofSize: 10, weight: .thin)
    public var rect: CGRect = .zero
    
    init(arrow: Arrow) {
        self.arrow = arrow
        self.name = arrow.rawValue
        // The name is how we hash a Drawable object. Since arrows don't have unique names and are not reused, let's just use the time hash.
        self.hash = String(Date().timeIntervalSince1970)
    }
    
    func drawLine(path: UIBezierPath) {
        arrow.configureLine(path: path)
    }
    
}

public class Relationship {
    public let from: Object
    public var relates: Relates
    public let to: Object
    
    init(from: Object, relates: Relates, to: Object) {
        self.from = from
        self.relates = relates
        self.to = to
    }
}

public enum Constants: String, CaseIterable {
    case label = "l"
    case stroke = "s"
    case fill = "f"
    case width = "w"
    case font = "F"
}

public class Diagram {
    public var objects: [Object] = []
    public var relationships: [Relationship] = []
    
    func indexOf(name: String) -> Int? {
        return objects.firstIndex{ $0.name == name }
    }

    func objectOf(name: String) -> Object? {
        if let index = (objects.firstIndex{ $0.name == name }) {
            return objects[index]
        }
        return nil
    }
    
    func updateObject(name: String, with object: Object) {
        if let index = (objects.firstIndex{ $0.name == name }) {
            objects[index] = object
        } else {
            objects.append(object)
        }
    }
    
    public var description: String {
        var output = ""
        
        for (i, r) in relationships.enumerated() {
            output += "\n\(i): \(r.from.description) \(r.relates.description) \(r.to.description)"
        }
        
        return output
    }
}


