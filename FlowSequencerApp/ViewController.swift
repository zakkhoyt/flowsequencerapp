//
//  ViewController.swift
//  FlowSequencerApp
//
//  Created by Zakk Hoyt on 12/21/18.
//  Copyright © 2018 Zakk Hoyt. All rights reserved.
//

import UIKit
import FlowSequencer

class ViewController: UIViewController {

    var reader: DiagramReader?
    
    @IBOutlet weak var fourierView: FourierView!
    @IBOutlet weak var fourierSegment: UISegmentedControl!
    @IBOutlet weak var fourierIterationsStepper: UIStepper!
    @IBOutlet weak var fourierIterationsLabel: UILabel!
    
    
    @IBOutlet weak var trigView: TrigView!
    @IBOutlet weak var trigFunctionSegment: UISegmentedControl!
    
    
    @IBOutlet var diagramView: DiagramView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        do {
            reader = try DiagramReader(name: "flow00")
            if let diagram = reader?.diagram {
                print("reader.diagram:\n\(diagram.description)")
                diagramView.diagram = diagram
            }
        } catch {
            print("Caught error: \(error)")
        }
        
        setupFourierSegment()
        setupFourierStepper()
        setupTrigFunctionSegment()
    }
    
    private func setupFourierSegment() {
        fourierSegment.removeAllSegments()
        
        for (i, waveform) in FourierView.Waveform.allCases.enumerated() {
            fourierSegment.insertSegment(withTitle: waveform.title, at: i, animated: false)
        }
        fourierSegment.selectedSegmentIndex = FourierView.Waveform.square.rawValue
        fourierSegmentValueChanged(fourierSegment)
    }
    
    private func setupFourierStepper() {
        fourierIterationsLabel.textColor = .lightGray
        fourierIterationsStepper.minimumValue = 1
        fourierIterationsStepper.maximumValue = 50
        fourierIterationsStepper.value = 2
        fourierIterationsStepper.stepValue = 1.0
        fourierIterationsStepperValueChanged(fourierIterationsStepper)
    }
    
    private func setupTrigFunctionSegment() {
        trigFunctionSegment.removeAllSegments()
        for (i, trigFunction) in TrigView.TrigFunction.allCases.enumerated() {
            trigFunctionSegment.insertSegment(withTitle: trigFunction.title, at: i, animated: false)
        }
        trigFunctionSegment.selectedSegmentIndex = TrigView.TrigFunction.sin.rawValue
        
    }

    @IBAction func fourierSegmentValueChanged(_ sender: UISegmentedControl) {
        guard let waveform = FourierView.Waveform(rawValue: sender.selectedSegmentIndex) else { return }
        fourierView.waveform = waveform
    }
    
    @IBAction func fourierIterationsStepperValueChanged(_ sender: UIStepper) {
        fourierView.iterations = Int(sender.value)
        fourierIterationsLabel.text = "Iterations: \(fourierView.iterations)"
    }
    
    @IBAction func trigFunctionSegmentValueChanged(_ sender: UISegmentedControl) {
        guard let trigFunction = TrigView.TrigFunction(rawValue: sender.selectedSegmentIndex) else { return }
        if trigView.trigFunctions.contains(trigFunction) {
            trigView.trigFunctions.remove(trigFunction)
        } else {
            trigView.trigFunctions.insert(trigFunction)
        }
        
        // Deselect the control after a bit
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            sender.selectedSegmentIndex = -1
        }
    }
    
    
}

